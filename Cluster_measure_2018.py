# -*- coding: utf-8 -*-
"""
Created on Thu Dec 27 10:06:17 2018

Refined cluster size and nearest neighbours detector

@author: Yufeng
"""
import numpy as np
import scipy.ndimage as nd
import pandas as pd
from skimage import io
import PYME.Analysis.thresholding as thresh
import os
import matplotlib.pyplot as plt
"""create a dictionary for storing the dataframe
the dataframe will contain all the key parameters for the entire analysis
a second dataframe will be there for super cluster measures

index|size|nearestneighbour|region|imgname"""

dir = "img dir"
dir2 = "mask dir"
dir3 = "output dir"

def batch(dir, dir2, dir3, scdist = 10, min_size = 36):
    dfnormal = pd.DataFrame()
    dfnormalsum = pd.DataFrame()
    dfsupercluster = pd.DataFrame()
    for filename in os.listdir(dir):
        
        if filename.endswith(".tif"):
            filename = filename.split('.')[0]
            
            print ('processing image {}'.format(filename))
            
            
            tempdf1, tempdf2= main_seq(dir, dir2, dir3, filename, scdist, min_size)
            dfnormal = dfnormal.append(tempdf1)
            dfsupercluster = dfsupercluster.append(tempdf2)
            

            continue
        else:
            continue
    dfnormal.to_excel(dir + 'clusteranalysis' +'.xls')
    dfnormalsum = dfnormal.groupby(['Region', 'Treatment', 'image name']).mean()
    dfnormalsd = dfnormal.groupby(['Region', 'Treatment', 'image name']).std()
    dfnormalsum.to_excel(dir + 'clusteranalysissummary' +'.xls')
    dfnormalsd.to_excel(dir + 'clusteranalysissummarysd' +'.xls')
    
    dfsupercluster.to_excel(dir + 'clusteranalysisSC' +'.xls')
    dfscsum = dfsupercluster.groupby(['Region', 'Treatment', 'image name']).mean()
    dfscsd = dfsupercluster.groupby(['Region', 'Treatment', 'image name']).std()
    dfscsum.to_excel(dir + 'clusteranalysisSCsummary' +'.xls')
    dfscsd.to_excel(dir + 'clusteranalysisSCsummarysd' +'.xls')
    return dfnormal, dfsupercluster

def main_seq(dir, dir2, dir3, fname, scdist = 10, min_size = 36):
    treat = 'Normal'
    if fname[0] == 'D':
        treat = 'Failing'
    
    img = io.imread(dir + fname + '.tif')
    imgthresh = img > thresh.signalFraction(img, 0.8)
    imgthresh2 = imgthresh.astype(np.int8)
    io.imsave(dir3 + fname + 'thresh.tif', imgthresh2)

    
    mask_tt = io.imread(dir2 + fname + 'ttmsk.tif')
    mask_sl = io.imread(dir2 + fname + 'slmsk.tif')
    dfn= normal_cluster_measure(fname, treat, imgthresh, mask_sl, mask_tt, min_size)
    dfsc = super_cluster_measure(fname, treat, imgthresh, mask_sl, mask_tt, min_size, scdist)
    return dfn, dfsc
    
    
def create_circular_mask(h, w, center=None, radius=None):

    if center is None: # use the middle of the image
        center = [int(w/2), int(h/2)]
    if radius is None: # use the smallest distance between the center and image walls
        radius = min(center[0], center[1], w-center[0], h-center[1])

    Y, X = np.ogrid[:h, :w]
    dist_from_center = np.sqrt((X - center[0])**2 + (Y-center[1])**2)

    mask = dist_from_center <= radius
    return mask
    

def normal_cluster_measure(fname, treat, img, mask_sl, mask_tt, min_size = 36):
    
    """binarise mask data"""
    img = img/np.max(img)
    mask_sl = mask_sl/np.max(mask_sl)
    mask_tt = mask_tt/np.max(mask_tt)
    
    """label the mask"""
    label, nobjs = nd.label(img)
    
    """size filter"""
    sizes = nd.sum(img,label,np.arange(nobjs+1))
    
    for i in range(1,nobjs+1):
        if sizes[i] < min_size:
            img[label == i] = 0
    
    """relabel"""
    label, nobjs = nd.label(img)
    size_data = nd.sum(img,label,np.arange(1, nobjs+1))
    print (len(size_data))
    
    """nearest neighbours"""
    dists = np.zeros(nobjs)
    
    for objnum in range(1,nobjs+1):

        print ('checking object {} of {}'.format(objnum, nobjs+1))
        # one example with object number 20
        curlabel = objnum
    
        dist = nd.distance_transform_edt(label != curlabel)
        otherobjs = (label != curlabel) & (label > 0)
        
    
        # the distance of the other objects looked up from the dist20 distance transform
        objdists = dist[otherobjs]
        # we get the nearest object distance as the minimum of all distances
        mindist = objdists.min()
    
        dists[objnum-1] = mindist
    """when filtering for mask regons remember to include (objno-1)"""
    """mask filtering"""
    region = np.repeat('corbular', len(size_data))
    cell = np.repeat(fname, len(size_data))
    group = np.repeat(treat, len(size_data))
    ttclusters = np.unique(label * mask_tt)
    region[ttclusters-1] = 't-tubular'
    sarcclusters = np.unique(label * mask_sl)
    region[sarcclusters-1] = 'sarcolemmal'
    
    df = pd.DataFrame([])
    df['Size'] = size_data/36
    df['Nearest Neighbour'] = dists*5
    df['Region'] = region
    df['image name'] = cell
    df['Treatment'] = group

    return df

def super_cluster_measure(fname, treat, img, mask_sl, mask_tt, min_size = 36, scdist = 10):
    
    """binarise mask data"""
    img = img/np.max(img)
    mask_sl = mask_sl/np.max(mask_sl)
    mask_tt = mask_tt/np.max(mask_tt)
    
    """scmask"""
    kernel = create_circular_mask(scdist, scdist)
    #scmask = nd.binary_dilation(img, kernel)
    
    """label the mask"""
    label, nobjs = nd.label(img)
    #label = label * img


    
    """size filter"""
    sizes = nd.sum(img,label,np.arange(nobjs+1))
    
    for i in range(1,nobjs+1):
        if sizes[i] < min_size:
            img[label == i] = 0
    
    """relabel"""
    scmask = nd.binary_dilation(img, kernel)
    label, nobjs = nd.label(scmask)
    label = label * img
    
    size_data = nd.sum(img,label,np.arange(1, nobjs+1))
    print (len(size_data))
    
    """nearest neighbours"""
    dists = np.zeros(nobjs)
    scsize = np.zeros(nobjs)
    
    for objnum in range(1,nobjs+1):

        print ('checking object {} of {}'.format(objnum, nobjs+1))
        # one example with object number 20
        curlabel = objnum
        l, obj = nd.label(label == curlabel)
        scsize[objnum-1] = obj
    
        dist = nd.distance_transform_edt(label != curlabel)
        otherobjs = (label != curlabel) & (label > 0)
        
    
        # the distance of the other objects looked up from the dist20 distance transform
        objdists = dist[otherobjs]
        # we get the nearest object distance as the minimum of all distances
        mindist = objdists.min()
    
        dists[objnum-1] = mindist
    """when filtering for mask regons remember to include (objno-1)"""
    """mask filtering"""
    region = np.repeat('corbular', len(size_data))
    cell = np.repeat(fname, len(size_data))
    group = np.repeat(treat, len(size_data))
    ttclusters = np.unique(label * mask_tt)
    region[ttclusters-1] = 't-tubular'
    sarcclusters = np.unique(label * mask_sl)
    region[sarcclusters-1] = 'sarcolemmal'
    
    df = pd.DataFrame([])
    df['Size'] = size_data/36 #conversion to nRyR
    df['Nearest Neighbour'] = dists*5 #conversion to nm
    df['scsize'] = scsize
    df['Region'] = region
    df['image name'] = cell
    df['Treatment'] = group
    return df
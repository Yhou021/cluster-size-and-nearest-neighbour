# Cluster Size and nearest neighbour

scripts for measurement of RyR2 cluster size and distances to their nearest neighbours for different regions of the human myocyte

## Requirements:  
Python 2.7 or Python 3  
Numpy  
Scipy - ndimages  
Sci-kit image (skimage)  
Pandas  
Python Microscopy (https://python-microscopy.org/)
  
## Usage:
run or import Cluster_measure_2018 within ide of choice  

arrange files into 3 folders:  

- one for masks of the separate regions  
- one for the images to be processed  
- one for outputs  

**NB: All files should be in tiff format**

additionally, the script will separate groups (healthy, failing/diseased) based on the first letter of the input filename:  
- files starting with 'D' will be grouped as diseased/failing  
- files starting with other letters will be set as 'control'  
  
t-tubule masks should have the same filename as input images with 'ttmsk' suffix added.  
sarcolemma masks should have the same filename as input images with 'slmsk' suffix added.  

to operate:  
 run function "batch(dir, dir2, dir3, scdist = 10, min_size = 36)" specifying:  
 dir - directory (as str) of the files to be analysed  
 dir2 - directory of the masks  
 dir3 - directory of the output  
 scdist - half distance for super clusters (in pixels)  
 min_size - minimum size of the clusters (pixels^2)  
  
 results will be output to dir3  

     



